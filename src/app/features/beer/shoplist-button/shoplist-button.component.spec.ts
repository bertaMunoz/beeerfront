import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShoplistButtonComponent } from './shoplist-button.component';

describe('ShoplistButtonComponent', () => {
  let component: ShoplistButtonComponent;
  let fixture: ComponentFixture<ShoplistButtonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShoplistButtonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShoplistButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/_services/auth-services/auth.service';
import { TokenStorageService } from 'src/app/_services/auth-services/token-storage.service';
import { FavorisService } from 'src/app/_services/outils-services/favoris.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  currentUser = this.authService.currentUserValue;
  isLoggedIn = false;
  isLoginFailed = false;
  form: any = {
    username: '',
    password: '',
  };

  errorMessage = '';
  roles: string[] = [];

  constructor(
    private authService: AuthService,
    private router: Router,
    private tokenStorage: TokenStorageService,
    private favorisSerice: FavorisService
  ) { }

  ngOnInit(): void {
    if (this.tokenStorage.getToken()) {
      this.isLoggedIn = true;
      this.roles = this.tokenStorage.getUser().roles;
    }
  }

  login(): void {
    const { username, password } = this.form;
    this.authService.login(username, password).subscribe({
      next: (data) => {
        this.tokenStorage.saveToken(data.accessToken);
        this.tokenStorage.saveUser(data);
        this.isLoginFailed = false;
        this.isLoggedIn = true;
        this.roles = this.tokenStorage.getUser().roles;
        if (this.currentUser) {
          this.favorisSerice.initFavoris(this.currentUser.id);
        }

        console.log('connecté');
      },
      error: (err) => {
        console.log(err);
        this.isLoginFailed = true;
        this.errorMessage = err.error.message;
      },
      complete: () => {
        this.router.navigateByUrl('');
      },
    });
  }
}

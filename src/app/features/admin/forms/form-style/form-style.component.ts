import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Style } from 'src/app/models/models-impl/style.models';
import { StyleService } from 'src/app/_services/services-impl/style.service';

@Component({
  selector: 'app-form-style',
  templateUrl: './form-style.component.html',
  styleUrls: ['./form-style.component.css'],
})
export class FormStyleComponent implements OnInit {
  constructor(private fb: FormBuilder, private service: StyleService) { }
  errorMessage!: '';
  style!: Style;
  form = this.fb.group({
    name: 'IPA',
  });

  onSubmit() {
    this.style = this.form.getRawValue();
    this.service.create(this.style).subscribe({
      next: (data) => {
        console.log(data);
      },
      error: (err) => {
        this.errorMessage = err.error.message;
      },
      complete: () => { },
    });
  }

  ngOnInit(): void { }
}

import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Brewery } from 'src/app/models/models-impl/brewery.models';
import { BreweryService } from 'src/app/_services/services-impl/brewery.service';

@Component({
  selector: 'app-form-brewery',
  templateUrl: './form-brewery.component.html',
  styleUrls: ['./form-brewery.component.css'],
})
export class FormBreweryComponent implements OnInit {
  constructor(private fb: FormBuilder, private service: BreweryService) { }
  errorMessage!: '';
  brewery!: Brewery;
  form = this.fb.group({
    name: 'Brasserie Amplepuis',
  });

  onSubmit() {
    this.brewery = this.form.getRawValue();
    this.service.create(this.brewery).subscribe({
      next: (data) => {
        console.log(data);
      },
      error: (err) => {
        this.errorMessage = err.error.message;
      },
      complete: () => { },
    });
    console.log(this.form.value);
  }

  ngOnInit(): void { }
}

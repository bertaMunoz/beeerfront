import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'shoplist-button',
  templateUrl: './shoplist-button.component.html',
  styleUrls: ['./shoplist-button.component.css']
})
export class ShoplistButtonComponent implements OnInit {
  @Output() wishEvent = new EventEmitter<any>();
  @Input() isWish!: boolean;
  wishColor = 'var(--secondary-color)';
  unWishColor = 'var(--primary-color-dark)';
  constructor() { }

  ngOnInit(): void { }
  setShopList() {
    this.wishEvent.emit();
  }

}

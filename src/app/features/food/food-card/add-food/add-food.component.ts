import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Food } from 'src/app/models/models-impl/food.model';
import { Country } from 'src/app/models/models-impl/country.models';
import { FoodService } from 'src/app/_services/services-impl/food.service';
import { CountryService } from 'src/app/_services/services-impl/country.service';

@Component({
  selector: 'app-add-food',
  templateUrl: './add-food.component.html',
  styleUrls: ['./add-food.component.css'],
})
export class AddFoodComponent implements OnInit {
  addFood: FormGroup;
  countries!: Country[];

  constructor(
    private fb: FormBuilder,
    private countryService: CountryService,
    private foodService: FoodService,
    @Inject(MAT_DIALOG_DATA) public data: Food
  ) {
    this.addFood = this.fb.group({
      name: ['', Validators.required, Validators.minLength(4)],
      img: ['', Validators.required],
      description: ['', Validators.required],
      country: this.fb.group({
        id: '',
      }),
    });
  }

  ngOnInit(): void {
    this.getCountries();
  }

  onSubmit(): void {
    console.log(this.addFood.value);
    this.foodService.create(this.addFood.value).subscribe({
      next: (data) => {},
    });
  }

  getCountries() {
    this.countryService.findAll().subscribe((data) => {
      this.countries = data;
      console.log('liste pays', data);
    });
  }
}

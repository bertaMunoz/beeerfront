import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/models-impl/user.model';
import { UserService } from 'src/app/_services/services-impl/user.service';
import { faUserCircle } from '@fortawesome/free-solid-svg-icons';
import { AuthService } from 'src/app/_services/auth-services/auth.service';

@Component({
  selector: 'app-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.css'],
})
export class ProfilComponent implements OnInit {
  faUser = faUserCircle;
  currentUserId = this.authService.currentUserValue.id;
  currentUser: User = this.authService.currentUserValue;
  user!: User;
  errorMessage = '';
  updatingEmail!: boolean;
  updatingPwd!: boolean;

  constructor(
    private userService: UserService,
    private authService: AuthService
  ) {}

  initProfil() {
    this.userService.getProfil(this.currentUserId).subscribe({
      next: (data) => {
        this.user = data;
      },
      error: (err) => {
        this.errorMessage = err.error.message;
      },
    });
  }

  update(string: string) {
    if (string == 'email') {
      this.updatingEmail = true;
      this.updatingPwd = false;
    } else if (string == 'pwd') {
      this.updatingPwd = true;
      this.updatingEmail = false;
    }
  }

  returnToProfilCard() {
    console.log('initprofil');

    this.updatingEmail = false;
    this.updatingPwd = false;
    this.initProfil();
  }

  ngOnInit(): void {
    this.initProfil();
    this.updatingEmail = false;
    this.updatingPwd = false;
  }
}

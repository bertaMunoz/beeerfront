import { Component, Input, OnInit } from '@angular/core';
import { AuthService } from 'src/app/_services/auth-services/auth.service';

@Component({
  selector: 'app-food-card',
  templateUrl: './food-card.component.html',
  styleUrls: ['./food-card.component.css'],
})
export class FoodCardComponent implements OnInit {
  @Input() food: any;

  currentUser: any;
  userIsConnected!: boolean;

  constructor(private authService: AuthService) {}

  ngOnInit(): void {
    this.getUserStatus();
  }

  getUserStatus() {
    this.currentUser = this.authService.currentUserValue;
    console.log('test2: ', this.currentUser);
    if (this.currentUser != null) {
      this.userIsConnected = true;
    } else {
      this.userIsConnected = false;
    }
  }
}

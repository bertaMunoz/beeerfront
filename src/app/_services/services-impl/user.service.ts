import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Beer } from 'src/app/models/models-impl/beer.models';
import { environment } from 'src/environments/environment';
import { User } from '../../models/models-impl/user.model';
import { GenericService } from '../generic.service';

const API_URL = 'http://localhost:8080/beer_appetit/test/';

@Injectable({
  providedIn: 'root',
})
export class UserService extends GenericService<User> {
  constructor(private http: HttpClient) {
    super(http, User, environment.API_URL, 'users');
  }

  getPublicContent(): Observable<any> {
    return this.http.get(API_URL + 'all', { responseType: 'text' });
  }
  getUserBoard(): Observable<any> {
    return this.http.get(API_URL + 'user', { responseType: 'text' });
  }
  getModeratorBoard(): Observable<any> {
    return this.http.get(API_URL + 'mod', { responseType: 'text' });
  }
  getAdminBoard(): Observable<any> {
    return this.http.get(API_URL + 'admin', { responseType: 'text' });
  }
  getProfil(id: string): Observable<any> {
    return this.http.get<User>(`${environment.API_URL}/users/${id}`);
  }

  updateEmail(id: string, email: string) {
    return this.http.patch(`${environment.API_URL}/users/${id}/email`, email);
  }
  updatePwd(id: string, passwords: any) {
    return this.http.patch(`${environment.API_URL}/users/${id}/pwd`, {
      passwords,
    });
  }

  /**
   * 
   * @param id 
   * @param beer 
   * @returns 
   */
  setFavori(id: string, beer: Beer) {
    return this.http.post(
      `${environment.API_URL}/users/${id}/beers/favoris`,
      beer
    );
  }

  /**
   * 
   * @param id 
   * @returns 
   * Permet de recuperer les favoris d'un user
   */
  getFavoris(id: string) {
    return this.http.get<Beer[]>(
      `${environment.API_URL}/users/${id}/beers/favoris`
    );
  }

  setShoplist(id: string, beer: Beer) {
    return this.http.post(
      `${environment.API_URL}/users/${id}/beers/shoplist`,
      beer
    );
  }

  getShoplist(id: string) {
    return this.http.get<Beer[]>(
      `${environment.API_URL}/users/${id}/beers/shoplist`
    );
  }
}

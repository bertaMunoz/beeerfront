import { Brewery } from './brewery.models';
import { Color } from './color.models';
import { Country } from './country.models';
import { GenericModel } from '../generic.models';
import { Style } from './style.models';

export class Beer extends GenericModel<Beer> {
  name!: string;
  alcool!: string;
  appearance!: string;
  aroma!: string;
  img!: string;
  taste!: string;
  color!: Color;
  country!: Country;
  style!: Style;
  brewery!: Brewery;
  isLike!: boolean;
  isWish!: boolean;

  constructor(model?: Partial<Beer>) {
    super(model);
  }
}

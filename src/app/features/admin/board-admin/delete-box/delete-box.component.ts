import { Component, Inject, Optional } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Beer } from 'src/app/models/models-impl/beer.models';

@Component({
  selector: 'app-delete-box',
  templateUrl: './delete-box.component.html',
  styleUrls: ['./delete-box.component.css'],
})
export class DeleteBoxComponent {
  action: string;
  local_data: any;
  updateSize = '25%';
  constructor(
    public dialogRef: MatDialogRef<DeleteBoxComponent>,
    //@Optional() is used to prevent error if no data is passed
    @Optional() @Inject(MAT_DIALOG_DATA) public data: Beer
  ) {
    console.log(data);
    this.local_data = { ...data };
    this.action = this.local_data.action;
  }
  doAction() {
    this.dialogRef.close({ event: this.action, data: this.local_data });
  }

  closeDialog() {
    this.dialogRef.close({ event: 'Cancel' });
  }
  deleteElement() {
    this.dialogRef.close({ event: 'Delete', data: this.local_data });
  }
}

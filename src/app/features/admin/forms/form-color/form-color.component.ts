import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Color } from 'src/app/models/models-impl/color.models';
import { ColorService } from 'src/app/_services/services-impl/color.service';

@Component({
  selector: 'app-form-color',
  templateUrl: './form-color.component.html',
  styleUrls: ['./form-color.component.css'],
})
export class FormColorComponent implements OnInit {
  constructor(private fb: FormBuilder, private service: ColorService) { }
  errorMessage!: '';
  color!: Color;
  form = this.fb.group({
    name: 'Blonde',
  });

  onSubmit() {
    this.color = this.form.getRawValue();
    this.service.create(this.color).subscribe({
      next: (data) => {
        console.log(data);
      },
      error: (err) => {
        this.errorMessage = err.error.message;
      },
      complete: () => { },
    });
    console.log(this.form.value);
  }

  ngOnInit(): void { }
}

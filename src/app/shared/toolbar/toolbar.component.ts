import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/_services/auth-services/auth.service';
import { FavorisService } from 'src/app/_services/outils-services/favoris.service';
import { ShopListService } from 'src/app/_services/outils-services/shop-list.service';
@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.css'],
})
export class ToolbarComponent implements OnInit {
  private roles: string[] = [];
  showAdminBoard = false;
  showMemberBoard = false;
  currentUser: any;
  favorisTotal!: number;
  favroisSub!: Subscription;
  wishCount!: number;
  wishSub!: Subscription;


  constructor(
    private authService: AuthService,
    private _favorisService: FavorisService,
    private _shopListService: ShopListService
  ) { }

  ngOnInit(): void {
    this.authService.currentUser.subscribe({
      next: (data) => {
        this.currentUser = data;
        if (this.currentUser) {
          this.roles = this.currentUser.roles;
          this.showAdminBoard = this.roles.includes('ROLE_ADMIN');
          this.showMemberBoard = this.roles.includes('ROLE_MEMBER');
        }
      },
      error: (err) => {
        console.log(err.error);
      },
    });
    this.favroisSub = this._favorisService
      .getFavorisCount()
      .subscribe((favorisCount) => {
        this.favorisTotal = favorisCount;

      });
    this.wishSub = this._shopListService
      .getWishCount()
      .subscribe(count => {
        this.wishCount = count;
      })
  }

  logout() {
    this.authService.logout();
    this.showAdminBoard = false;
    this.showMemberBoard = false;
  }
  ngOnDestroy() {
    this.favroisSub.unsubscribe();
    this.wishSub.unsubscribe();
  }
}

import { GenericModel } from '../generic.models';

export class Color extends GenericModel<Color> {
  public name!: string;
  constructor(model?: Partial<Color>) {
    super(model);
  }
}

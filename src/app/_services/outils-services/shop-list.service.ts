import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Beer } from '../../models/models-impl/beer.models';
import { AuthService } from '../auth-services/auth.service';
import { UserService } from '../services-impl/user.service';

@Injectable({
  providedIn: 'root'
})
export class ShopListService {
  currentUser: any = this.authservice.currentUserValue;
  private _wishCount = new BehaviorSubject<number>(0);
  private _wishCount$ = this._wishCount.asObservable();

  private _shopList = new BehaviorSubject<Beer[]>([]);
  private _shopList$ = this._shopList.asObservable();

  constructor(
    private userService: UserService,
    private authservice: AuthService) {
    this.currentUser = this.authservice.currentUserValue;
    if (this.currentUser) {
      this.initShoplist(this.currentUser.id);
    }
  }

  getWishCount(): Observable<number> {
    return this._wishCount$;
  }

  setWishCount(lastValue: any) {
    return this._wishCount.next(lastValue);
  }

  getShopList(): Observable<Beer[]> {
    return this._shopList$;
  }

  setShopList(beerList: Beer[]) {
    this.setWishCount(beerList.length);
    return this._shopList.next(beerList);
  }

  initShoplist(userId: string) {
    this.userService.getShoplist(userId).subscribe({
      next: (response) => {
        this.setShopList(response);
      },
      error: (err) => { },
      complete: () => { },
    });
  }
  addOrRemoveBeerFromShoplist(userId: string, beer: Beer) {
    this.userService.setShoplist(userId, beer).subscribe({
      next: (response) => {
        this.setShopList(response as Beer[]);
      },
    });
  }

}

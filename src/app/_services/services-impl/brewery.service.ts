import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Brewery } from 'src/app/models/models-impl/brewery.models';
import { environment } from 'src/environments/environment';
import { GenericService } from '../generic.service';

@Injectable({
  providedIn: 'root',
})
export class BreweryService extends GenericService<Brewery> {
  constructor(private http: HttpClient) {
    super(http, Brewery, environment.API_URL, 'breweries');
  }
}

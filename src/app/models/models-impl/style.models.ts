import { GenericModel } from '../generic.models';

export class Style extends GenericModel<Style> {
  public name!: string;
  constructor(model?: Partial<Style>) {
    super(model);
  }
}

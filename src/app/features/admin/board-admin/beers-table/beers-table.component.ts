import { Component, OnInit } from '@angular/core';
import { ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable, MatTableDataSource } from '@angular/material/table';
import { Beer } from 'src/app/models/models-impl/beer.models';
import { ToastService } from 'src/app/_services/outils-services/toast.service';
import { BeerService } from 'src/app/_services/services-impl/beer.service';
import { DeleteBoxComponent } from '../delete-box/delete-box.component';
import { BeerFormComponent } from '../../../beer/beer-form/beer-form.component';

@Component({
  selector: 'app-beers-table',
  templateUrl: './beers-table.component.html',
  styleUrls: ['./beers-table.component.css'],
})
export class BeersTableComponent implements OnInit {
  beers!: any[];
  errorMessage = '';
  dataSource!: MatTableDataSource<Beer>;
  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatTable, { static: true }) table!: MatTable<any>;

  constructor(private service: BeerService, public dialog: MatDialog, private toastService: ToastService) { }

  displayedColumns: string[] = [
    'name',
    'color',
    'style',
    'alcool',
    'brewery',
    'country',
    'actions',
  ];

  initBeers() {
    this.service.findAll().subscribe({
      next: (response) => {
        this.dataSource = new MatTableDataSource(response);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      },
      error: (err) => {
        this.errorMessage = err.error.message;
      },
      complete: () => { },
    });
  }
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  ngOnInit(): void {
    this.initBeers();
  }

  openDeleteDialog(action: string, element: any): void {
    const dialogRef = this.dialog.open(DeleteBoxComponent, {
      width: '40%',
      height: '30%',
      backdropClass: 'custom-dialog-backdrop-class',
      panelClass: 'custom-dialog-panel-class',
      data: element,
    });

    dialogRef.afterClosed().subscribe({
      next: (result) => {
        if (result && result.event == 'edit') {
          this.updateBeer(result.data),
            this.toastService.openSnackBar("Bière " + element.name + " supprimée!!", "", "end", "bottom", 500)
        }
      }, error: () => { },
      complete: () => {

      }
    });
  }

  openEditDialog(action: string, element: any) {
    const dialogRef = this.dialog.open(BeerFormComponent, {
      width: '70%',
      height: '80%',
      backdropClass: 'custom-dialog-backdrop-class',
      panelClass: 'edit-dialog-box',
      data: element,
    });

    dialogRef.afterClosed().subscribe({
      next: (result) => {
        if (result && result.event == 'edit') {
          this.updateBeer(result.data),
            this.toastService.openSnackBar("Bière Mise à jour.", "Merci!!", "end", "bottom", 500)
        }
      }, error: () => { },
      complete: () => { }
    });
  }
  deleteBeer(id: string) {
    this.service.delete(id).subscribe({
      next: () => {
        console.log('Bière supprimée');
      },
      error: (err) => {
        this.errorMessage = err.error.message;
      },
      complete: () => {
        this.initBeers();
      },
    });
  }

  updateBeer(beer: Beer) {
    this.service.update(beer).subscribe({
      next: () => {
        console.log('Bière mise à jour');
      },
      error: (err) => {
        this.errorMessage = err.error.message;
      },
      complete: () => {
        this.initBeers();
      },
    });
  }
}

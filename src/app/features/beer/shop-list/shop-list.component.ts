import { Component, OnInit } from '@angular/core';
import { Beer } from 'src/app/models/models-impl/beer.models';
import { AuthService } from 'src/app/_services/auth-services/auth.service';
import { ShopListService } from 'src/app/_services/outils-services/shop-list.service';

@Component({
  selector: 'app-shop-list',
  templateUrl: './shop-list.component.html',
  styleUrls: ['./shop-list.component.css']
})
export class ShopListComponent implements OnInit {
  currentUser: any = this.authService.currentUserValue;
  shopList: Beer[] = [];
  currentUserId = this.authService.currentUserValue.id;
  errorMessage = '';
  constructor(
    private authService: AuthService,
    private shopListService: ShopListService
  ) { }

  ngOnInit(): void {
    this.shopListService.getShopList().subscribe({
      next: (beers) => {
        this.shopList = beers
      }, complete: () => { }
    });
  }
  setShopList(beer: Beer) {
    this.shopListService.addOrRemoveBeerFromShoplist(this.currentUserId, beer);
  }

}

import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { Beer } from 'src/app/models/models-impl/beer.models';
import { FavorisService } from 'src/app/_services/outils-services/favoris.service';
import { ShopListService } from 'src/app/_services/outils-services/shop-list.service';
import { BeerFormComponent } from '../../beer-form/beer-form.component';

@Component({
  selector: 'app-beer-list',
  templateUrl: './beer-list.component.html',
  styleUrls: ['./beer-list.component.css']
})
export class BeerListComponent implements OnInit {
  @Input() public currentUser: any
  @Input() public beers: Beer[] | null = null
  errorMessage = '';
  beerCount!: number;

  favorisTotal!: number;
  subscription!: Subscription;
  nameFilter: string = '';
  events: string[] = [];
  sideNavOpened: boolean = false;
  constructor(
    private favorisService: FavorisService,
    private shopListService: ShopListService,
    private dialogRef: MatDialog,) { }

  ngOnInit(): void {
  }

  setFavori(beer: Beer) {
    this.favorisService.addOrRemoveBeerFromFavoris(this.currentUser.id, beer);
  }

  setShopList(beer: Beer) {
    this.shopListService.addOrRemoveBeerFromShoplist(this.currentUser.id, beer)
  }
  openDialog() {
    let diag = this.dialogRef.open(BeerFormComponent, {
      data: {},
      width: '70%',
      height: '80%',
    });
    diag.afterClosed().subscribe((result) => {
      // this.initBeers();
    });
  }

  filter(type: string, value: string | string[]) {
    switch (type) {
      case "name":
        this.filterByName(value as string)
        break;
      case "color":
        this.colorFilter(value as string[])
        break;
      case "country":
        this.countryFilter(value as string[])
        break;
      case "style":
        this.styleFilter(value as string[])
        break;

      default:
        break;
    }

  }
  filterByName(value: string) {
    // value = value.trim();
    // value = value.toLowerCase();

    // let filterBeer: Beer[] = [];
    // this.beers.filter((b) => {
    //   let name = b.name.toLowerCase();
    //   if (name.includes(value)) {
    //     filterBeer.push(b);
    //   }
    // });
    // this.beers = filterBeer;
  }

  colorFilter(colorIds: string[]) {
    // if (colorIds.length == 0) { this.initBeers() } else {
    //   let filterBeer: Beer[] = [];
    //   this.beers.filter((beer) => {
    //     colorIds.map((colorId) => {
    //       if (beer.color.id == colorId) {
    //         filterBeer.push(beer)
    //       }
    //     })
    //   })
    //   this.beers = filterBeer;
    // }
  }

  countryFilter(countryIds: string[]) {
    // if (countryIds.length == 0) { this.initBeers() } else {
    //   let filterBeer: Beer[] = [];
    //   this.beers.filter((beer) => {
    //     countryIds.map((countryId) => {
    //       if (beer.country.id == countryId) {
    //         filterBeer.push(beer)
    //       }
    //     })
    //   })
    //   this.beers = filterBeer;
    // }
  }

  styleFilter(styleIds: string[]) {
    // if (styleIds.length == 0) { this.initBeers() } else {
    //   let filterBeer: Beer[] = [];
    //   this.beers.filter((beer) => {
    //     styleIds.map((styleId) => {
    //       if (beer.style.id == styleId) {
    //         filterBeer.push(beer)
    //       }
    //     })
    //   })
    //   this.beers = filterBeer;
    // }
  }
}

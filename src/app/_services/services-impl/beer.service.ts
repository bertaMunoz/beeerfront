import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, filter, Observable, tap, map } from 'rxjs';
import { Beer } from 'src/app/models/models-impl/beer.models';
import { Brewery } from 'src/app/models/models-impl/brewery.models';
import { environment } from 'src/environments/environment';
import { GenericService } from '../generic.service';

@Injectable({
  providedIn: 'root',
})
export class BeerService extends GenericService<Beer> {

  public beers$: BehaviorSubject<Beer[]> = new BehaviorSubject<Beer[]>([])

  constructor(private http: HttpClient) {
    super(http, Beer, environment.API_URL, 'beers');
  }

  fetchBeers(): Observable<Beer[]> {
    return this.http.get<Beer[]>(`${environment.API_URL}/beers`).pipe(
      tap((beers) => {
        this.beers$.next(beers)
      })
    )
  }

  getBeer(index: number): Observable<Beer> {
    return this.beers$.pipe(
      filter((beers) => beers !== null),
      map((beers: Beer[]) => {
        return beers[index]
      })
    )
  }

  createBeer(beer: Beer): Observable<Beer> {
    return this.http
      .post<Beer>(`${environment.API_URL}/beers`, beer)
      .pipe(
        tap((savedBeer: Beer) => {
          const value = this.beers$.value;
          this.beers$.next([...value, savedBeer]);
        })
      );
  }

  public editBeer(
    beerId: string | undefined,
    editedBeer: Beer
  ): Observable<Beer> {
    return this.http
      .patch<Beer>(
        `${environment.API_URL}/beers/${beerId}`,
        editedBeer
      )
      .pipe(
        tap((savedBeer: Beer) => {
          const value = this.beers$.value;
          this.beers$.next(
            value.map((beer: Beer) => {
              if (beer.name === savedBeer.name) {
                return savedBeer;
              } else {
                return beer;
              }
            })
          );
        })
      );
  }


}

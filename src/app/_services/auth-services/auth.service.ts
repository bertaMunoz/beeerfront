import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, map, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
};
@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private currentUserSubjet: BehaviorSubject<any>;
  public currentUser: Observable<any>;

  constructor(private http: HttpClient, private router: Router) {
    const localUser = localStorage.getItem('currentUser');
    if (localUser) {
      this.currentUserSubjet = new BehaviorSubject<any>(JSON.parse(localUser));
    } else {
      this.currentUserSubjet = new BehaviorSubject<any>(null);
    }
    this.currentUser = this.currentUserSubjet.asObservable();
  }

  get currentUserValue() {
    return this.currentUserSubjet.value;
  }

  login(username: string, password: string): Observable<any> {
    return this.http
      .post(
        `${environment.API_URL}/auth/login`,
        { username, password },
        httpOptions
      )
      .pipe(
        map((user) => {
          localStorage.setItem('currentUser', JSON.stringify(user));
          this.currentUserSubjet.next(user);
          return user;
        })
      );
  }

  register(
    firstname: string,
    lastname: string,
    email: string,
    password: string,
    nickname: string
  ): Observable<any> {
    return this.http.post(
      `${environment.API_URL}/auth/register`,
      {
        firstname,
        lastname,
        email,
        password,
        nickname,
      },
      httpOptions
    );
  }

  logout() {
    localStorage.removeItem('currentUser');
    this.currentUserSubjet.next(null);
    this.router.navigateByUrl('login');
    console.log('Logout');
  }
}

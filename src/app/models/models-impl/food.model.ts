import { GenericModel } from '../generic.models';
import { Country } from './country.models';

export class Food extends GenericModel<Food> {
  name!: string;
  img!: string;
  description!: string;
  country!: Country;

  constructor(model?: Partial<Food>) {
    super(model);
  }
}

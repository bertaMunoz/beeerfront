import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Beer } from 'src/app/models/models-impl/beer.models';
import { AuthService } from '../auth-services/auth.service';
import { BeerService } from '../services-impl/beer.service';
import { UserService } from '../services-impl/user.service';

@Injectable({
  providedIn: 'root'
})
export class FilterService {
  currentUser: any = this.authservice.currentUserValue;
  private _beerList = new BehaviorSubject<Beer[]>([]);
  private _beerList$ = this._beerList.asObservable();
  constructor(
    private userService: UserService,
    private beerService: BeerService,
    private authservice: AuthService) {
    this.initBeerList()

  }
  getBeerList(): Observable<Beer[]> {
    return this._beerList$;
  }

  setBeerList(beerList: Beer[]) {
    return this._beerList.next(beerList);
  }
  initBeerList() {
    this.beerService.findAll().subscribe({
      next: (response) => {
        this.setBeerList(response as Beer[]);


      },
    })
  }

}

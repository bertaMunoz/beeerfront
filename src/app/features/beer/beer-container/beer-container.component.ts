import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/_services/auth-services/auth.service';
import { BeerService } from 'src/app/_services/services-impl/beer.service';

@Component({
  selector: 'app-beer-container',
  templateUrl: './beer-container.component.html',
  styleUrls: ['./beer-container.component.css']
})
export class BeerContainerComponent implements OnInit {

  currentUser = this.authService.currentUserValue;
  public beers$ = this.beerService.beers$;

  constructor(private authService: AuthService, private beerService: BeerService) { }

  ngOnInit(): void {
  }

}

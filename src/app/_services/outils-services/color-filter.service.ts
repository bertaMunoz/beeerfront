import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ColorFilterService {
  colorsFiltered!: String[];
  private _colorFilter = new BehaviorSubject<String[]>(this.colorsFiltered);
  private _colorFilter$ = this._colorFilter.asObservable();

  getColorFilter(): Observable<String[]> {
    return this._colorFilter$;
  }

  setColorFilter(lastValue: any) {
    return this._colorFilter.next(lastValue);
  }
}

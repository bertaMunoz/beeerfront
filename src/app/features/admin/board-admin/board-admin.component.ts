import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatTable } from '@angular/material/table';
import { Style } from 'src/app/models/models-impl/style.models';
import { UserService } from 'src/app/_services/services-impl/user.service';
@Component({
  selector: 'app-board-admin',
  templateUrl: './board-admin.component.html',
  styleUrls: ['./board-admin.component.css'],
})
export class BoardAdminComponent implements OnInit {
  content?: string;
  gestionBDD: boolean = false;
  gestionUser: boolean = false;
  AddData: boolean = false;
  addColor: boolean = false;
  addStyle: boolean = false;
  addCountry: boolean = false;
  addbrewery: boolean = false;
  dialogValue!: string;
  sendValue!: string;
  @ViewChild(MatTable, { static: true }) table!: MatTable<any>;

  type: Object = Style;
  constructor(private userService: UserService, public dialog: MatDialog) { }
  ngOnInit(): void {
    this.userService.getAdminBoard().subscribe({
      next: (data) => {
        this.content = data;
      },
      error: (err) => {
        this.content = JSON.parse(err.error).message;
      },
    });
  }

  ////////////////////////////////////////////
}

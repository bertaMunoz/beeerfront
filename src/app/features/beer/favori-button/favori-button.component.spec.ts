import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FavoriButtonComponent } from './favori-button.component';

describe('FavoriButtonComponent', () => {
  let component: FavoriButtonComponent;
  let fixture: ComponentFixture<FavoriButtonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FavoriButtonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FavoriButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

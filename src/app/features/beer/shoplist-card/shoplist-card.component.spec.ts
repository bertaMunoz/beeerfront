import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShoplistCardComponent } from './shoplist-card.component';

describe('ShoplistCardComponent', () => {
  let component: ShoplistCardComponent;
  let fixture: ComponentFixture<ShoplistCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShoplistCardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShoplistCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

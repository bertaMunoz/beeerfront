import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AddFoodComponent } from 'src/app/features/food/food-card/add-food/add-food.component';
import { Food } from 'src/app/models/models-impl/food.model';
import { AuthService } from 'src/app/_services/auth-services/auth.service';
import { FoodService } from 'src/app/_services/services-impl/food.service';

@Component({
  selector: 'app-foods',
  templateUrl: './foods.component.html',
  styleUrls: ['./foods.component.css'],
})
export class FoodsComponent implements OnInit {
  currentUser: any
  foods: Food[] = [];

  newFood!: Food;

  constructor(private foodService: FoodService, private dialog: MatDialog, private authService: AuthService) { }

  ngOnInit(): void {
    this.initFoods();
    this.currentUser = this.authService.currentUserValue;

  }

  initFoods() {
    this.foodService.findAll().subscribe((data) => {
      this.foods = data;
      console.log('plats:' + data);
    });
  }

  openDialog() {
    const dialogRef = this.dialog.open(AddFoodComponent, {
      data: {},
    });
    dialogRef.afterClosed().subscribe((result) => {
      console.log('result: ', result);
      // this.foodService.create(result);
      this.initFoods();
    });
  }

  // closeDialog(){
  //   this.dialogR
  // }
}

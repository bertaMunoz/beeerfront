import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource, MatTable } from '@angular/material/table';
import { Food } from 'src/app/models/models-impl/food.model';
import { FoodService } from 'src/app/_services/services-impl/food.service';
import { DeleteBoxComponent } from '../delete-box/delete-box.component';

@Component({
  selector: 'app-foods-table',
  templateUrl: './foods-table.component.html',
  styleUrls: ['./foods-table.component.css'],
})
export class FoodsTableComponent implements OnInit {
  foods!: any[];
  errorMessage = '';
  dataSource!: MatTableDataSource<Food>;
  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatTable, { static: true }) table!: MatTable<any>;
  constructor(private foodservice: FoodService, public dialog: MatDialog) { }

  displayedColumns: string[] = [
    'name',
    'description',
    'country',
    'img',
    'actions',
  ];

  initFoods() {
    this.foodservice.findAll().subscribe({
      next: (response) => {
        this.dataSource = new MatTableDataSource(response);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      },
      error: (err) => {
        this.errorMessage = err.error.message;
      },
      complete: () => { },
    });
  }
  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  ngOnInit(): void {
    this.initFoods();
  }

  openDialog(action: string, element: any): void {
    const dialogRef = this.dialog.open(DeleteBoxComponent, {
      width: '40%',
      height: '30%',
      backdropClass: 'custom-dialog-backdrop-class',
      panelClass: 'custom-dialog-panel-class',
      data: element,
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result.event == 'Delete') {
        this.deleteRowData(result.data.id);
      }
    });
  }
  deleteRowData(id: string) {
    this.foodservice.delete(id).subscribe({
      next: () => {
        console.log('Plat supprimer');
      },
      error: (err) => {
        this.errorMessage = err.error.message;
      },
      complete: () => {
        this.initFoods();
      },
    });
  }
}

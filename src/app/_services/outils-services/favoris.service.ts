import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Beer } from 'src/app/models/models-impl/beer.models';
import { AuthService } from '../auth-services/auth.service';
import { UserService } from '../services-impl/user.service';

@Injectable({
  providedIn: 'root',
})
export class FavorisService {
  currentUser: any = this.authservice.currentUserValue;
  private _favorisCount = new BehaviorSubject<number>(0);
  private _favorisCount$ = this._favorisCount.asObservable();

  private _favorisList = new BehaviorSubject<Beer[]>([]);
  private _favorisList$ = this._favorisList.asObservable();

  constructor(
    private userService: UserService,
    private authservice: AuthService) {
    this.currentUser = this.authservice.currentUserValue;
    if (this.currentUser) {
      this.initFavoris(this.currentUser.id);
    }
  }

  getFavorisCount(): Observable<number> {
    return this._favorisCount$;
  }

  setFavorisCount(lastValue: any) {
    return this._favorisCount.next(lastValue);
  }

  getFavorisList(): Observable<Beer[]> {
    return this._favorisList$;
  }

  setFavorisList(beerList: Beer[]) {
    this.setFavorisCount(beerList.length);
    return this._favorisList.next(beerList);
  }

  initFavoris(userId: string) {
    this.userService.getFavoris(userId).subscribe({
      next: (response) => {
        this.setFavorisList(response);
      },
      error: (err) => { },
      complete: () => {
      },
    });
  }
  addOrRemoveBeerFromFavoris(userId: string, beer: Beer) {
    this.userService.setFavori(userId, beer).subscribe({
      next: (response) => {
        this.setFavorisList(response as Beer[]);
      },
    });
  }
}

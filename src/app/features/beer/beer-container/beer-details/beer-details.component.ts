import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Beer } from 'src/app/models/models-impl/beer.models';
import { AuthService } from 'src/app/_services/auth-services/auth.service';
import { BeerService } from 'src/app/_services/services-impl/beer.service';

@Component({
  selector: 'app-beer-details',
  templateUrl: './beer-details.component.html',
  styleUrls: ['./beer-details.component.css']
})
export class BeerDetailsComponent implements OnInit {
  beer!: Beer;
  id: string;
  returnButton = { backgroundColor: 'var(--primary-color-light)' }
  currentUser: any;


  constructor(
    private service: BeerService,
    private activeRoute: ActivatedRoute,
    private authService: AuthService
  ) {
    this.id = this.activeRoute.snapshot.params['id'];
  }

  initBeer() {
    this.service.getById(this.id).subscribe((data) => (this.beer = data));
  }

  ngOnInit(): void {
    this.initBeer();
    this.authService.currentUser.subscribe({
      next: (data) => {
        this.currentUser = data;

      },
      error: (err) => {
        console.log(err.error);
      },
    });
  }
}

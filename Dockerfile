FROM node:16-alpine3.16 as builder
#Recuperer les fichier du projet
WORKDIR /app
#copy des fichiers
COPY . .
#installation des dépendence
RUN npm i
#build de l'application
RUN npm run build
#/app/dist/index.html

# On utilise un nouveau FROM pour importer une image de serveur Web.
# Puisqu'on à compilé l'application au dessus, nginx est capable de servir nativement tout le front.
FROM nginx:1.9.15-alpine

#recupérer l'applic buildé
COPY --from=builder /app/dist/ /usr/share/nginx/html
# la déplacer au bon endroit
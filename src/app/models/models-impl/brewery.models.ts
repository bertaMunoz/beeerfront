import { GenericModel } from '../generic.models';

export class Brewery extends GenericModel<Brewery> {
  public name!: string;
  constructor(model?: Partial<Brewery>) {
    super(model);
  }
}

import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Beer } from 'src/app/models/models-impl/beer.models';
import { AuthService } from 'src/app/_services/auth-services/auth.service';
import { FavorisService } from 'src/app/_services/outils-services/favoris.service';
import { ShopListService } from 'src/app/_services/outils-services/shop-list.service';

@Component({
  selector: 'app-shoplist-card',
  templateUrl: './shoplist-card.component.html',
  styleUrls: ['./shoplist-card.component.css']
})
export class ShoplistCardComponent implements OnInit {
  @Input() beer!: Beer;
  favoris!: Beer[];
  ShopList!: Beer[];
  currentUser: any = this.authService.currentUserValue;
  errorMessage = '';
  @Output() setFavoriEvent = new EventEmitter<Beer>();
  @Output() setShoplistEvent = new EventEmitter<Beer>();
  constructor(
    private authService: AuthService,
    private favorisService: FavorisService,
    private shopListService: ShopListService
  ) { }

  ngOnInit(): void {
    this.isFavoris();
    this.isInShopList();
  }

  setFavori() {
    this.setFavoriEvent.emit(this.beer);
    this.beer.isLike = !this.beer.isLike;
  }
  getFavoris() {
    this.favorisService.getFavorisList().subscribe({
      next: (data) => {
        this.favoris = data;
      },
    });
  }

  isFavoris() {
    this.getFavoris();
    if (this.favoris.some((f) => f.id === this.beer.id)) {
      this.beer.isLike = true;
    } else {
      this.beer.isLike = false;
    }
  }

  setBeerToShopList() {
    this.setShoplistEvent.emit(this.beer);
    this.beer.isWish = !this.beer.isWish;
  }
  getShopList() {
    this.shopListService.getShopList().subscribe({
      next: (data) => {
        this.ShopList = data;
      },
    });
  }

  isInShopList() {
    this.getShopList();
    if (this.ShopList.some((f) => f.id === this.beer.id)) {
      this.beer.isWish = true;
    } else {
      this.beer.isWish = false;
    }
  }


}

import { GenericModel } from '../generic.models';

export class Country extends GenericModel<Country> {
  public name!: string;
  constructor(model?: Partial<Country>) {
    super(model);
  }
}

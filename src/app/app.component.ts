import { Component, HostListener, OnInit } from '@angular/core';
import { BeerService } from './_services/services-impl/beer.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  title = 'Beer Apettit';
  constructor(
    private beerService: BeerService) {
  }

  ngOnInit(): void {
    this.beerService.fetchBeers().subscribe();
  }

  /*
  @HostListener('window:beforeunload', ['$event'])
  beforeUnloadHandler() {
    window.sessionStorage.clear();
    window.localStorage.clear();
  }
  */

}

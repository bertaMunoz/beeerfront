import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Country } from 'src/app/models/models-impl/country.models';
import { CountryService } from 'src/app/_services/services-impl/country.service';

@Component({
  selector: 'app-form-country',
  templateUrl: './form-country.component.html',
  styleUrls: ['./form-country.component.css'],
})
export class FormCountryComponent implements OnInit {
  constructor(private fb: FormBuilder, private service: CountryService) { }
  errorMessage!: '';
  country!: Country;
  form = this.fb.group({
    name: 'France',
  });

  onSubmit() {
    this.country = this.form.getRawValue();
    this.service.create(this.country).subscribe({
      next: (data) => {
        console.log(data);
      },
      error: (err) => {
        this.errorMessage = err.error.message;
      },
      complete: () => { },
    });
    console.log(this.form.value);
  }

  ngOnInit(): void { }
}

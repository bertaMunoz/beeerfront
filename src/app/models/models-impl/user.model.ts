import { GenericModel } from '../generic.models';
import { Beer } from './beer.models';

export class User extends GenericModel<User> {
  constructor(model?: Partial<User>) {
    super(model);
    this.createdAt = new Date().toISOString().slice(0, 16);
    this.updatedAt = new Date().toISOString().slice(0, 16);
  }

  firstname!: string;
  lastname!: string;
  email!: string;
  nickname!: string;
  password!: string;
  createdAt?: string;
  updatedAt?: string;
  favoris?: Beer[];
  shopList?: Beer[];
}

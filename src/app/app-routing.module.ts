import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FavoriListComponent } from './features/beer/favori-list/favori-list.component';
import { BeerContainerComponent } from './features/beer/beer-container/beer-container.component';
import { BeerDetailsComponent } from './features/beer/beer-container/beer-details/beer-details.component';
import { BoardAdminComponent } from './features/admin/board-admin/board-admin.component';
import { FoodsDetailComponent } from './features/food/food-card/foods-detail/foods-detail.component';
import { FoodsComponent } from './features/food/food-card/foods/foods.component';
import { HomeComponent } from './features/home/home.component';
import { LoginComponent } from './features/user/login/login.component';
import { ShopListComponent } from './features/beer/shop-list/shop-list.component';
import { RegisterComponent } from './features/user/register/register.component';
import { ProfilComponent } from './features/user/profil/profil.component';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'beers', component: BeerContainerComponent },
  { path: 'beers/:id', component: BeerDetailsComponent },
  { path: 'favoris', component: FavoriListComponent },
  { path: 'shoplist', component: ShopListComponent },
  { path: 'foods', component: FoodsComponent },
  { path: 'foods/:id', component: FoodsDetailComponent },

  { path: 'profil', component: ProfilComponent },



  // { path: 'beers', component: BeersComponent },
  // { path: 'beers/favoris', component: FavoriListComponent },
  // { path: 'beers/shoplist', component: ShopListComponent },
  // { path: 'beerDetail/:id', component: BeersDetailComponent },
  // { path: 'foods', component: FoodsComponent },
  // { path: 'foodDetail/:id', component: FoodsDetailComponent },

  { path: '', component: HomeComponent },

  { path: 'admin', component: BoardAdminComponent },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }

import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'favori-button',
  templateUrl: './favori-button.component.html',
  styleUrls: ['./favori-button.component.css'],
})
export class FavoriButtonComponent implements OnInit {
  @Output() likeEvent = new EventEmitter<any>();
  @Input() isLike!: boolean;
  likeColor = 'var(--secondary-color)';
  unLikeColor = 'var(--primary-color-dark)';
  constructor() { }

  ngOnInit(): void { }
  setFavori() {
    this.likeEvent.emit();
  }
}

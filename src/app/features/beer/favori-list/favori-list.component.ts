import { Component, OnInit } from '@angular/core';
import { Beer } from 'src/app/models/models-impl/beer.models';
import { AuthService } from 'src/app/_services/auth-services/auth.service';
import { FavorisService } from 'src/app/_services/outils-services/favoris.service';
import { UserService } from 'src/app/_services/services-impl/user.service';

@Component({
  selector: 'app-favori-list',
  templateUrl: './favori-list.component.html',
  styleUrls: ['./favori-list.component.css'],
})
export class FavoriListComponent implements OnInit {
  localUser = localStorage.getItem('currentUser');
  favorisList: Beer[] = [];
  currentUserId = this.authService.currentUserValue.id;
  errorMessage = '';
  constructor(
    private authService: AuthService,
    private favorisService: FavorisService,
    private userService: UserService
  ) { }

  ngOnInit(): void {
    this.userService.getFavoris(this.currentUserId).subscribe({
      next: (favoris) => {
        this.favorisList = favoris;
      },
    });
    //   this.favorisService.getFavorisList().subscribe({
    //     next: (favoris) => {
    //       this.favorisList = favoris;
    //     },
    //   });
  }
  setFavori(beer: Beer) {
    this.favorisService.addOrRemoveBeerFromFavoris(this.currentUserId, beer);
  }
}

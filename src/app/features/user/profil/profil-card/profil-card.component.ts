import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { User } from 'src/app/models/models-impl/user.model';

import { faUserCircle } from '@fortawesome/free-solid-svg-icons';
import { AuthService } from 'src/app/_services/auth-services/auth.service';

@Component({
  selector: 'app-profil-card',
  templateUrl: './profil-card.component.html',
  styleUrls: ['./profil-card.component.css'],
})
export class ProfilCardComponent implements OnInit {
  faUser = faUserCircle;
  @Input() user!: User;
  @Input() updatingEmail!: boolean;
  @Input() updatingPwd!: boolean;
  @Output() updateEvent = new EventEmitter<string>();

  constructor(private authService: AuthService) {}

  update(string: string) {
    this.updateEvent.emit(string);
  }

  ngOnInit(): void {}
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormBreweryComponent } from './form-brewery.component';

describe('FormBreweryComponent', () => {
  let component: FormBreweryComponent;
  let fixture: ComponentFixture<FormBreweryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormBreweryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormBreweryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

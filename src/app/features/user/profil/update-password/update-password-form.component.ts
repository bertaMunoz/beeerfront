import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { User } from 'src/app/models/models-impl/user.model';
import { AuthService } from 'src/app/_services/auth-services/auth.service';
import { UserService } from 'src/app/_services/services-impl/user.service';

@Component({
  selector: 'app-update-password-form',
  templateUrl: './update-password-form.component.html',
  styleUrls: ['./update-password-form.component.css'],
})
export class UpdatePasswordFormComponent implements OnInit {
  @Input() user!: User;
  @Output() backEvent = new EventEmitter<string>();
  pwdForm: FormGroup = this.fb.group({
    oldPassword: '',
    newPassword: ['', [Validators.required, Validators.minLength(6)]],
  });
  currentUserId = this.authService.currentUserValue.id;
  errorMessage = '';

  constructor(
    private fb: FormBuilder,
    private userService: UserService,
    private authService: AuthService
  ) {}

  returnToProfilCard() {
    this.backEvent.emit();
  }

  updatePassword() {
    console.log(this.pwdForm.value);

    this.userService
      .updatePwd(this.currentUserId, this.pwdForm.value)
      .subscribe({
        next: (data) => console.log(data),
        error: (err) => console.log(err),
        complete: () => this.returnToProfilCard(),
      });
  }
  ngOnInit(): void {}
}

import { Injectable } from '@angular/core';
import {
  MatSnackBar,
  MatSnackBarHorizontalPosition,
  MatSnackBarVerticalPosition,
} from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root',
})
export class ToastService {
  constructor(private _snackBar: MatSnackBar) { }

  openSnackBar(
    message: string,
    action: string,
    posH: MatSnackBarHorizontalPosition,
    posV: MatSnackBarVerticalPosition,
    duration: number
  ) {
    this._snackBar.open(message, action, {
      horizontalPosition: posH,
      verticalPosition: posV,
      duration: duration + 1000,
      panelClass: ['customsnack']
    });
  }
}

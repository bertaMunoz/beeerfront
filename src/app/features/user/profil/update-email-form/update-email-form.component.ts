import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from 'src/app/models/models-impl/user.model';
import { AuthService } from 'src/app/_services/auth-services/auth.service';
import { UserService } from 'src/app/_services/services-impl/user.service';

@Component({
  selector: 'app-update-email-form',
  templateUrl: './update-email-form.component.html',
  styleUrls: ['./update-email-form.component.css'],
})
export class UpdateEmailFormComponent implements OnInit {
  @Input() user!: User;
  @Output() backEvent = new EventEmitter<string>();
  currentUserId = this.authService.currentUserValue.id;
  emailForm: FormGroup = this.fb.group({
    username: ['', Validators.email],
  });
  errorMessage = '';

  constructor(
    private fb: FormBuilder,
    private userService: UserService,
    private authService: AuthService,
    private router: Router
  ) {}

  returnToProfilCard() {
    this.backEvent.emit();
  }

  updateEmail() {
    const email = this.emailForm.getRawValue().username;
    this.userService.updateEmail(this.currentUserId, email).subscribe({
      next: (data) => console.log(data),
      error: (err) => console.log(err),
      complete: () => this.returnToProfilCard(),
    });
  }
  ngOnInit(): void {}
}

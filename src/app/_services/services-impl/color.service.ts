import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Color } from 'src/app/models/models-impl/color.models';
import { environment } from 'src/environments/environment';
import { GenericService } from '../generic.service';

@Injectable({
  providedIn: 'root',
})
export class ColorService extends GenericService<Color> {
  constructor(private http: HttpClient) {
    super(http, Color, environment.API_URL, 'colors');
  }
}

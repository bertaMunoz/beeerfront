import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/_services/auth-services/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
})
export class RegisterComponent implements OnInit {
  form: any = {
    firstname: 'nico',
    lastname: 'gaget',
    username: null,
    email: 'nico@mail.com',
    password: 'password',
    nickname: 'beaujoland',
  };

  isSuccessful = false;
  isSignUpFailed = false;
  errorMessage = '';

  constructor(private authService: AuthService) {}

  ngOnInit(): void {}

  onSubmit(): void {
    const { firstname, lastname, email, password, nickname } = this.form;
    this.authService
      .register(firstname, lastname, email, password, nickname)
      .subscribe({
        next: (data) => {
          console.log(data);
          this.isSuccessful = true;
          this.isSignUpFailed = false;
        },
        error: (err) => {
          this.errorMessage = err.error.message;
          this.isSignUpFailed = true;
        },
        /*complete: () => {
          this.router.navigateByUrl('home');
        }
        */
      });
  }
}

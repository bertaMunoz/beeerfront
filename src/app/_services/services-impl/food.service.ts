import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Food } from '../../models/models-impl/food.model';
import { GenericService } from '../generic.service';

@Injectable({
  providedIn: 'root',
})
export class FoodService extends GenericService<Food> {
  constructor(private http: HttpClient) {
    super(http, Food, environment.API_URL, 'foods');
  }
}

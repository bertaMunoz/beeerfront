import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { MaterialModule } from 'src/app/shared/_modules/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { authInterceptorProviders } from './_helpers/auth.interceptor';
import { AppComponent } from './app.component';
import { BoardAdminComponent } from './features/admin/board-admin/board-admin.component';
import { HomeComponent } from './features/home/home.component';
import { LoginComponent } from './features/user/login/login.component';
import { BeerCardComponent } from './features/beer/beer-card/beer-card.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ProfilCardComponent } from './features/user/profil/profil-card/profil-card.component';
import { UpdateEmailFormComponent } from './features/user/profil/update-email-form/update-email-form.component';
import { UpdatePasswordFormComponent } from './features/user/profil/update-password/update-password-form.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FoodsComponent } from './features/food/food-card/foods/foods.component';
import { FoodCardComponent } from './features/food/food-card/food-card.component';
import { AddFoodComponent } from './features/food/food-card/add-food/add-food.component';

import { FavoriButtonComponent } from './features/beer/favori-button/favori-button.component';
import { FoodsDetailComponent } from './features/food/food-card/foods-detail/foods-detail.component';
import { AddBreweryComponent } from './features/beer/add-brewery/add-brewery.component';
import { SideFilterComponent } from './features/beer/side-filter/side-filter.component';
import { ShopListComponent } from './features/beer/shop-list/shop-list.component';
import { FavoriListComponent } from './features/beer/favori-list/favori-list.component';
import { ShoplistCardComponent } from './features/beer/shoplist-card/shoplist-card.component';
import { BeerContainerComponent } from './features/beer/beer-container/beer-container.component';
import { BeerListComponent } from './features/beer/beer-container/beer-list/beer-list.component';
import { BeerDetailsComponent } from './features/beer/beer-container/beer-details/beer-details.component';
import { BeersTableComponent } from './features/admin/board-admin/beers-table/beers-table.component';
import { DeleteBoxComponent } from './features/admin/board-admin/delete-box/delete-box.component';
import { FormBreweryComponent } from './features/admin/forms/form-brewery/form-brewery.component';
import { FormColorComponent } from './features/admin/forms/form-color/form-color.component';
import { FormCountryComponent } from './features/admin/forms/form-country/form-country.component';
import { FormStyleComponent } from './features/admin/forms/form-style/form-style.component';
import { UsersTableComponent } from './features/admin/board-admin/users-table/users-table.component';
import { ProfilComponent } from './features/user/profil/profil.component';
import { RegisterComponent } from './features/user/register/register.component';
import { FoodsTableComponent } from './features/admin/board-admin/foods-table/foods-table.component';
import { ShoplistButtonComponent } from './features/beer/shoplist-button/shoplist-button.component';
import { ToolbarComponent } from './shared/toolbar/toolbar.component';
import { BeerFormComponent } from './features/beer/beer-form/beer-form.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    RegisterComponent,
    LoginComponent,
    BeerCardComponent,
    BoardAdminComponent,
    ProfilComponent,
    ProfilCardComponent,
    UpdateEmailFormComponent,
    UpdatePasswordFormComponent,
    ToolbarComponent,
    FormCountryComponent,
    FormStyleComponent,
    FormColorComponent,
    FormBreweryComponent,
    BeersTableComponent,
    DeleteBoxComponent,
    FavoriListComponent,
    FavoriButtonComponent,
    FoodsComponent,
    FoodsDetailComponent,
    FoodCardComponent,
    AddFoodComponent,
    AddBreweryComponent,
    SideFilterComponent,
    UsersTableComponent,
    FoodsTableComponent,
    BeerFormComponent,
    ShopListComponent,
    ShoplistButtonComponent,
    ShoplistCardComponent,
    BeerContainerComponent,
    BeerListComponent,
    BeerDetailsComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MaterialModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    FontAwesomeModule,
    FlexLayoutModule,
  ],
  providers: [authInterceptorProviders],
  bootstrap: [AppComponent],
  exports: []
})
export class AppModule { }

import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Brewery } from 'src/app/models/models-impl/brewery.models';
import { BreweryService } from 'src/app/_services/services-impl/brewery.service';

@Component({
  selector: 'app-add-brewery',
  templateUrl: './add-brewery.component.html',
  styleUrls: ['./add-brewery.component.css'],
})
export class AddBreweryComponent implements OnInit {
  breweries!: Brewery[];
  brewery!: Brewery;

  breweryForm = this.fb.group({
    name: 'brasserie',
  });
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: Brewery,
    private dialogRef: MatDialogRef<AddBreweryComponent>,
    private fb: FormBuilder,
    private breweryServ: BreweryService
  ) {}

  ngOnInit(): void {}
  addBrewery() {
    this.breweryServ.create(this.breweryForm.value).subscribe({
      next: (data) => {
        //emit de la data
        this.closeDialog(data as Brewery);

        console.log('id de la brass');

        console.log(data.id);
        console.log(data.name);
      },
      error: (err) => {},
    });
  }
  closeDialog(brewery: Brewery) {
    this.dialogRef.close(brewery);
  }
}
/*
addBrewery() {
    console.log(this.breweryForm.value);
    this.breweryServ.create(this.breweryForm.value).subscribe({
      next: (data) => {
        console.log("id de la brass");
        
        console.log(data);
        this.createdBrewId = data;
      },
      error: (err) => {}
    });
  }
*/
// @Output() breweryEvent = new EventEmitter<Brewery>();

// @Input() createdBrew!: Brewery;

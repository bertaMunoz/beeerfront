import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Food } from 'src/app/models/models-impl/food.model';
import { AuthService } from 'src/app/_services/auth-services/auth.service';
import { FoodService } from 'src/app/_services/services-impl/food.service';

@Component({
  selector: 'app-foods-detail',
  templateUrl: './foods-detail.component.html',
  styleUrls: ['./foods-detail.component.css'],
})
export class FoodsDetailComponent implements OnInit {
  food!: Food;
  id: string;

  currentUser: any;
  userIsConnected!: boolean;

  constructor(
    private service: FoodService,
    private activatedRoute: ActivatedRoute,
    private authService: AuthService
  ) {
    this.id = this.activatedRoute.snapshot.params['id'];
  }

  ngOnInit(): void {
    this.initFood();
    this.getUserStatus();
  }

  initFood() {
    this.service.getById(this.id).subscribe((data) => {
      this.food = data;
    });
  }

  getUserStatus() {
    this.currentUser = this.authService.currentUserValue;
    console.log('test2: ', this.currentUser);
    if (this.currentUser != null) {
      this.userIsConnected = true;
    } else {
      this.userIsConnected = false;
    }
  }
}

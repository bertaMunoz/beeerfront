import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Style } from 'src/app/models/models-impl/style.models';
import { environment } from 'src/environments/environment';
import { GenericService } from '../generic.service';

@Injectable({
  providedIn: 'root',
})
export class StyleService extends GenericService<Style> {
  constructor(private http: HttpClient) {
    super(http, Style, environment.API_URL, 'styles');
  }
}

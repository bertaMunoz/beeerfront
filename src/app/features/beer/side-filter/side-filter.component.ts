import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatExpansionPanel } from '@angular/material/expansion';
import { Color } from 'src/app/models/models-impl/color.models';
import { Country } from 'src/app/models/models-impl/country.models';
import { Style } from 'src/app/models/models-impl/style.models';
import { ColorService } from 'src/app/_services/services-impl/color.service';
import { CountryService } from 'src/app/_services/services-impl/country.service';
import { StyleService } from 'src/app/_services/services-impl/style.service';

@Component({
  selector: 'app-side-filter',
  templateUrl: './side-filter.component.html',
  styleUrls: ['./side-filter.component.css'],
  viewProviders: [MatExpansionPanel]
})
export class SideFilterComponent implements OnInit {

  @Output() nameFilterEvent = new EventEmitter<string>();
  @Output() colorFilterEvent = new EventEmitter<string[]>()
  @Output() countryFilterEvent = new EventEmitter<string[]>()
  @Output() styleFilterEvent = new EventEmitter<string[]>()
  @Output() resetFormEvent = new EventEmitter<any>();

  styles: Style[] = [];
  colors: Color[] = [];
  countries: Country[] = [];
  colorArrayFilter?: string[];
  countryArrayFilter?: string[];
  styleArrayFilter?: string[];
  searchNameValue?: string;
  panelOpenState = false;

  constructor(
    private fb: FormBuilder,
    private styleService: StyleService,
    private countryService: CountryService,
    private colorService: ColorService
  ) { }


  beerForm: FormGroup = this.fb.group({
    name: '',
  });
  styleForm: FormGroup = this.fb.group({
    checkStyleArray: this.fb.array([]),
  });
  colorForm: FormGroup = this.fb.group({
    checkColorArray: this.fb.array([]),
  });
  countryForm: FormGroup = this.fb.group({
    checkCountryArray: this.fb.array([]),
  });

  newFiltermethode(type: string, event: any) {

  }

  onCheckBoxChange(type: string, event: any) {
    switch (type) {
      case "name":
        this.nameFilter(event)
        break;
      case "color":
        this.colorChange(event)
        break;
      case "country":
        this.countryChange(event)
        break;
      case "style":
        this.styleChange(event)
        break;

      default:
        break;
    }
  }

  colorChange(e: any) {
    const checkColorArray: FormArray = this.colorForm.get(
      'checkColorArray'
    ) as FormArray;
    if (e.target.checked) {
      checkColorArray.push(new FormControl(e.target.value));
    } else {
      let i: number = 0;
      checkColorArray.controls.forEach((item: any) => {
        if (item.value == e.target.value) {
          checkColorArray.removeAt(i);
          return;
        }
        i++;
      });
    }
    this.colorArrayFilter = checkColorArray.getRawValue();
    this.colorFilterEvent.emit(this.colorArrayFilter);
  }
  countryChange(e: any) {
    const checkCountryArray: FormArray = this.countryForm.get(
      'checkCountryArray'
    ) as FormArray;
    if (e.target.checked) {
      checkCountryArray.push(new FormControl(e.target.value));
    } else {
      let i: number = 0;
      checkCountryArray.controls.forEach((item: any) => {
        if (item.value == e.target.value) {
          checkCountryArray.removeAt(i);
          return;
        }
        i++;
      });
    }
    this.countryArrayFilter = checkCountryArray.getRawValue();
    this.countryFilterEvent.emit(this.countryArrayFilter);
  }

  styleChange(e: any) {
    const checkStyleArray: FormArray = this.styleForm.get(
      'checkStyleArray'
    ) as FormArray;

    if (e.target.checked) {
      checkStyleArray.push(new FormControl(e.target.value));
    } else {
      let i: number = 0;
      checkStyleArray.controls.forEach((item: any) => {
        if (item.value == e.target.value) {
          checkStyleArray.removeAt(i);
          return;
        }
        i++;
      });
    }
    this.styleArrayFilter = checkStyleArray.getRawValue();
    this.styleFilterEvent.emit(this.styleArrayFilter);
  }

  nameFilter(value: string) {
    this.searchNameValue = value;
    this.nameFilterEvent.emit(value);
    this.beerForm.reset();
  }

  resetFilter() {
    this.colorArrayFilter = [];
    this.countryArrayFilter = [];
    this.styleArrayFilter = [];
    this.searchNameValue = '';
    this.resetFormEvent.emit();

  }

  getStyles() {
    this.styleService.findAll().subscribe({
      next: (response) => {
        this.styles = response;
      },
    });
  }
  getColors() {
    this.colorService.findAll().subscribe({
      next: (response) => {
        this.colors = response;
      },
    });
  }
  getCountries() {
    this.countryService.findAll().subscribe({
      next: (response) => {
        this.countries = response;
      },
    });
  }

  ngOnInit(): void {
    this.getStyles();
    this.getColors();
    this.getCountries();


  }
}

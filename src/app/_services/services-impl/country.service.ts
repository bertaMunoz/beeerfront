import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Country } from 'src/app/models/models-impl/country.models';
import { environment } from 'src/environments/environment';
import { GenericService } from '../generic.service';

@Injectable({
  providedIn: 'root',
})
export class CountryService extends GenericService<Country> {
  constructor(private http: HttpClient) {
    super(http, Country, environment.API_URL, 'countries');
  }
}

import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable, MatTableDataSource } from '@angular/material/table';
import { User } from 'src/app/models/models-impl/user.model';
import { UserService } from 'src/app/_services/services-impl/user.service';
import { DeleteBoxComponent } from '../delete-box/delete-box.component';

@Component({
  selector: 'app-users-table',
  templateUrl: './users-table.component.html',
  styleUrls: ['./users-table.component.css'],
})
export class UsersTableComponent implements OnInit {
  users: User[] = [];
  dataSource!: MatTableDataSource<User>;
  errorMessage = '';
  @ViewChild(MatSort) sort!: MatSort;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatTable, { static: true }) table!: MatTable<any>;

  constructor(private userService: UserService, public dialog: MatDialog) { }
  displayedColumns: string[] = [
    'email',
    'nickname',
    'name',
    'createdAt',
    'updatedAt',
    'actions',
  ];

  initUsers() {
    this.userService.findAll().subscribe({
      next: (response) => {
        this.dataSource = new MatTableDataSource(response);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      },
      error: (err) => {
        this.errorMessage = err.error.message;
      },
      complete: () => { },
    });
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }

  openDialog(action: string, element: any): void {
    const dialogRef = this.dialog.open(DeleteBoxComponent, {
      width: '40%',
      height: '30%',
      backdropClass: 'custom-dialog-backdrop-class',
      panelClass: 'custom-dialog-panel-class',
      data: element,
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result.event == 'Delete') {
        this.deleteRowData(result.data.id);
      }
    });
  }
  deleteRowData(id: string) {
    this.userService.delete(id).subscribe({
      next: () => {
        console.log('Bière supprimée');
      },
      error: (err) => {
        this.errorMessage = err.error.message;
      },
      complete: () => {
        this.initUsers();
      },
    });
  }
  ngOnInit(): void {
    this.initUsers();
  }
}

import { Component, Inject, OnInit, Optional } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Beer } from 'src/app/models/models-impl/beer.models';
import { Brewery } from 'src/app/models/models-impl/brewery.models';
import { Color } from 'src/app/models/models-impl/color.models';
import { Country } from 'src/app/models/models-impl/country.models';
import { Style } from 'src/app/models/models-impl/style.models';
import { BreweryService } from 'src/app/_services/services-impl/brewery.service';
import { ColorService } from 'src/app/_services/services-impl/color.service';
import { CountryService } from 'src/app/_services/services-impl/country.service';
import { StyleService } from 'src/app/_services/services-impl/style.service';
import { DeleteBoxComponent } from '../../admin/board-admin/delete-box/delete-box.component';

@Component({
  selector: 'app-beer-form',
  templateUrl: './beer-form.component.html',
  styleUrls: ['./beer-form.component.css'],
})
export class BeerFormComponent implements OnInit {
  action: string;
  local_data: any;
  colors!: Color[];
  breweries!: Brewery[];
  countries!: Country[];
  styles!: Style[];
  img!: '';
  errorMessage = '';
  constructor(
    private colorServ: ColorService,
    private countryServ: CountryService,
    private breweryServ: BreweryService,
    private styleService: StyleService,
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<DeleteBoxComponent>,
    //@Optional() is used to prevent error if no data is passed
    @Optional() @Inject(MAT_DIALOG_DATA) public data: Beer
  ) {
    this.local_data = { ...data };
    this.action = this.local_data.action;
  }

  beerForm = this.fb.group({
    id: '',
    name: '',
    alcool: '',
    appearance: '',
    aroma: '',
    img: '',
    taste: '',
    color: this.fb.group({ id: '' }),
    brewery: this.fb.group({ id: '' }),
    country: this.fb.group({ id: '' }),
    style: this.fb.group({ id: '' }),
  });

  doAction() {
    this.dialogRef.close({ event: this.action, data: this.local_data });
  }

  closeDialog() {
    this.dialogRef.close({ event: 'Cancel' });
  }
  updateElement() {
    console.log(this.beerForm.value);

    this.dialogRef.close({ event: 'edit', data: this.beerForm.value });
  }

  initColors() {
    this.colorServ.findAll().subscribe((data) => {
      this.colors = data;
    });
  }
  initBrewery() {
    this.breweryServ.findAll().subscribe((data) => {
      this.breweries = data;
    });
  }
  initCountry() {
    this.countryServ.findAll().subscribe((data) => {
      this.countries = data;
    });
  }
  initStyle() {
    this.styleService.findAll().subscribe((data) => {
      this.styles = data;
    });
  }
  ngOnInit(): void {
    this.initColors();
    this.initBrewery();
    this.initCountry();
    this.initStyle();
    this.beerForm.patchValue(this.local_data);
    this.img = this.local_data.img;

  }
}

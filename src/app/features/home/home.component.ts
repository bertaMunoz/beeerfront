import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/_services/auth-services/auth.service';
import { FavorisService } from 'src/app/_services/outils-services/favoris.service';
import { UserService } from 'src/app/_services/services-impl/user.service';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  currentUser = this.authService.currentUserValue;
  content?: string;
  constructor(
    private userService: UserService,
    private authService: AuthService,
    private favorisService: FavorisService
  ) {}

  initUsers() {}
  ngOnInit(): void {
    this.userService.getPublicContent().subscribe({
      next: (data) => {
        this.content = data;
      },
      error: (err) => {
        this.content = JSON.parse(err.error).message;
      },
      complete: () => {
        if (this.currentUser) {
          this.favorisService.initFavoris(this.currentUser.id);
        }
      },
    });
  }
}

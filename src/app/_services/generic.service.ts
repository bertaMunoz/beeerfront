import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { GenericModel } from '../models/generic.models';

export abstract class GenericService<T extends GenericModel<T>> {
  constructor(
    private httpClient: HttpClient,
    private tConstructor: { new(m: Partial<T>, ...args: unknown[]): T },
    protected apiUrl: string,
    private endpoint: string
  ) { }
  public create(resource: T): Observable<T> {
    return this.httpClient
      .post<T>(`${this.apiUrl}/${this.endpoint}`, resource)
      .pipe(map((result) => new this.tConstructor(result)));
  }

  public findAll(): Observable<T[]> {
    return this.httpClient
      .get<T[]>(`${this.apiUrl}/${this.endpoint}`)
      .pipe(map((result) => result.map((i) => new this.tConstructor(i))));
  }

  public getById(id: string): Observable<T> {
    return this.httpClient
      .get<T>(`${this.apiUrl}/${this.endpoint}/${id}`)
      .pipe(map((result) => new this.tConstructor(result)));
  }

  public update(resource: Partial<T>): Observable<T> {
    return this.httpClient
      .put<T>(`${this.apiUrl}/${this.endpoint}/${resource.id}`, resource)
      .pipe(map((result) => new this.tConstructor(result)));
  }

  public delete(id: string): Observable<void> {
    return this.httpClient.delete<void>(
      `${this.apiUrl}/${this.endpoint}/${id}`
    );
  }
}
